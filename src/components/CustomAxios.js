import axios from "axios";
const http = axios.create({
    baseURL: 'https://api.exchangerate.host'
});

export default http;