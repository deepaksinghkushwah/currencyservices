import React, { useEffect, useState } from 'react'
import http from '../components/CustomAxios'
import Spinner from '../components/shared/Spinner';
function CurrencyConverter() {
  const [symbols, setSymbols] = useState([]);
  const [loading, setLoading] = useState(false);

  const [fromCurrency, setFromCurrency] = useState('');
  const [toCurrency, setToCurrency] = useState('');
  const [amount, setAmount] = useState(0);
  const [convertedAmount, setConvertedAmount] = useState(null);
  useEffect(() => {
    setLoading(true);
    const getSymbols = async () => {
      const sys = await http.get("/symbols");
      const data = await sys.data;
      let arr = Array.from(Object.keys(data.symbols), k => [`${k}`, data.symbols[k]]);
      setSymbols(arr);
      setLoading(false);
    };

    getSymbols();
  }, []);



  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);

    const res = await http.get('/convert?from=' + fromCurrency + '&to=' + toCurrency + "&amount=" + amount + "&places=2");
    const data = await res.data;
    //console.log(data);
    setConvertedAmount(data.result);
    setLoading(false);
  }

  const handleSelectChange = (e) => {
    setConvertedAmount(null);
    if(e.target.id === 'toCurrency'){
      setToCurrency(e.target.value);
    }

    if(e.target.id === 'fromCurrency'){
      setFromCurrency(e.target.value);
    }
  }

  if (loading) {
    return <Spinner />
  }
  return (
    <>
      <h1>Convert Currency</h1>
      <form onSubmit={handleSubmit}>
        <table className='table table-borderless table-hover'>
          <tbody>
            <tr>
              <td width="30%">From Currency</td>
              <td width="70%">
                <select className='form-control' id="fromCurrency" onChange={handleSelectChange}>
                  {symbols.length > 0 && symbols.map((item) => (
                    <option value={item[0]} key={item[1].code}>{item[1].code} - {item[1].description}</option>
                  ))}
                </select>
              </td>
            </tr>
            <tr>
              <td>To Currency</td>
              <td>
                <select className='form-control' id="toCurrency" onChange={handleSelectChange}>
                  {symbols.length > 0 && symbols.map((item) => (
                    <option value={item[0]} key={item[1].code}>{item[1].code} - {item[1].description}</option>
                  ))}
                </select>
              </td>
            </tr>
            <tr>
              <td>Amount</td>
              <td><input className='form-control' type="number" onChange={(e) => setAmount(e.target.value)} /></td>
            </tr>
          </tbody>
          <tfoot>
            <tr>
              <td>&nbsp;</td>
              <td><button className='btn btn-primary' type="submit">Convert</button></td>
            </tr>
            {convertedAmount !== null &&
              <tr>
                <td>Converted Amount</td>
                <td>{toCurrency} {convertedAmount}</td>
              </tr>
            }
          </tfoot>

        </table>
      </form>
    </>
  )
}

export default CurrencyConverter