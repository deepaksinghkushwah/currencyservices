import React from 'react'
import spinner from "../../assets/loading.gif";
function Spinner() {
    return (
        <div className='loadingSpinnerContainer'>
            <img src={spinner} alt="Loading..." width="200px" height="200px" />
        </div>
    )
}

export default Spinner