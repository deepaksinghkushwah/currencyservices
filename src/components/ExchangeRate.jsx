import React from 'react'
import Spinner from './shared/Spinner';

function ExchangeRate({ rates, loading }) {
    if (loading) {
        return <Spinner/>;
    }
    const listing = rates.length > 0 && (
        <div className="row row-cols-5 mt-3">
            {rates.map(item => (
                <div className="col mt-3 mb-1" key={item[0]}>
                    <div className="card bg-dark">
                        <div className="card-body">
                            <div className="card-text text-white">
                                {item[0]} - {item[1]}
                            </div>
                        </div>
                    </div>
                </div>
            ))}
        </div>
    )
    return (
        <>

            {listing}
        </>
    );

}

export default ExchangeRate