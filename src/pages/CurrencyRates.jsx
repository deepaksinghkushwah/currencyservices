import React, { useEffect, useState } from 'react'
import ExchangeRate from '../components/ExchangeRate'
import http from '../components/CustomAxios'
function CurrencyRates() {
  const [rates, setRates] = useState([]);
  const [baseCurrency, setBaseCurrency] = useState('Eur');
  const [loading, setLoading] = useState(true);
  const [exchangeDate, setExchangeDate] = useState('');
  useEffect(() => {
    const getRates = async () => {

      const response = await http.get('/latest?places=2');
      const data = await response.data;
      //console.log(data);
      let arr = Array.from(Object.keys(data.rates), k => [`${k}`, data.rates[k]]);
      //console.log(arr);
      setRates(arr);
      setLoading(false);
      setBaseCurrency(data.base);
      setExchangeDate(data.date);
    }
    getRates();

  }, [])
  return (
    <>
      <h1>Daily Exchange Rates</h1>
      <div className='text-bold'>
        <span className='float-start'><strong>Base Currency: {baseCurrency}</strong></span>
        <span className='float-end'><strong>Date: {exchangeDate}</strong></span>
        <br />
      </div>
      <ExchangeRate rates={rates} loading={loading} />
    </>

  )
}

export default CurrencyRates