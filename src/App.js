import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import Navigation from "./components/shared/Navigation";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import CurrencyConverter from "./pages/CurrencyConverter";
import CurrencyRates from "./pages/CurrencyRates";
function App() {
  return (
    <div className="App">
      <Router>
        <Navigation />
        <div className="container">
          <Routes>
            <Route path="/" element={<CurrencyRates />} />
            <Route path="/currency-converter" element={<CurrencyConverter />} />
          </Routes>
        </div>
      </Router>
    </div>
  );
}

export default App;
